Clone repo.
Open in R Interpreter.
Replace:

masterSid <- 'INSERT SID HERE'

with relevant sid.

Run all lines.

It'll spit out a .csv.

Half of the time you need to rerun from line 139 to the end as it will inaccurately treat every account as if it had 1 CPS the first time through. I can't for the life of me figure out why this is.


By default it runs on the past 7 days. You can change this by editing line 30.